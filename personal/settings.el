;; -*- mode: elisp -*-

(setq package-list '(color-theme color-theme-solarized yaml-mode handlebars-mode))
                                        ; bye bye splash screen
(setq inhibit-splash-screen t)

                                        ; enable syntax highlighting
(global-font-lock-mode t)
(transient-mark-mode 1)

(setq default-tab-width 4)
                                        ; Don't show that annoying "this is a symlink to a version-controlled
                                        ; file; are you sure?" prompt.
(setq vc-follow-symlinks t)
                                        ; Automatically reload buffers when they change on disk (N.B.: this
                                        ; won't happen if the file has unsaved modifications in emacs).
(global-auto-revert-mode 1)
                                        ; Always split vertically.
(setq split-width-threshold 0)
(setq split-height-threshold nil)

                                        ; Pretty deduplication of buffer names, instead of that <2>, <3>,
                                        ; etc. nonsense.
(require 'uniquify)
(setq
 uniquify-buffer-name-style 'post-forward
 uniquify-separator ":")


                                        ; Re-enable arrow keys.
(setq prelude-guru nil)

; Go away, flyspell.
(setq prelude-flyspell nil)

(require 'color-theme)
(color-theme-initialize)
(setq color-theme-is-global t)
                                        ; or run emacs with TERM=xterm-16color
(require 'color-theme-solarized)
(setq solarized-termcolors 16)
(color-theme-solarized-dark)

; https://github.com/bbatsov/projectile
(projectile-global-mode)
(setq projectile-enable-caching t)

; longer lines
(setq whitespace-line-column 120)
(setq-default fill-column 120)

;; (setq package-archives '(("elpa" . "http://tromey.com/elpa/")
;;                          ("melpa" . "http://melpa.milkbox.net/")))

; http://stackoverflow.com/questions/10092322/how-to-automatically-install-emacs-packages-by-specifying-a-list-of-package-name
;; (package-initialize)
;; (when (not package-archive-contents)
;;   (package-refresh-contents))
;; (dolist (package package-list)
;;   (when (not (package-installed-p package))
;;     (package-install package)))

;(load-theme 'solarized-dark t)
; (setq prelude-whitespace t) ; whitespace-mode makes spaces/tabs visible
